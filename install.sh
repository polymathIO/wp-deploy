sudo apt-get update

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

sudo apt-get install -y vim curl python-software-properties

gpg --keyserver hkp://keys.gnupg.net --recv-keys D39DC0E3
curl -sSL https://get.rvm.io | bash -s stable

source /usr/local/rvm/scripts/rvm
rvm use --install 1.9.3
shift
rvm use --install 2.0.0
shift



sudo add-apt-repository -y ppa:ondrej/php5
sudo apt-get update

sudo apt-get install -y make php5 apache2 libapache2-mod-php5 php5-curl php5-gd php5-mcrypt php5-readline mysql-server-5.5 php5-mysql git-core php5-xdebug sendmail npm

cat << EOF | sudo tee -a /etc/php5/mods-available/xdebug.ini
xdebug.scream=1
xdebug.cli_color=1
xdebug.show_local_vars=1
EOF

sudo a2enmod rewrite

sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php5/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php5/apache2/php.ini
sed -i "s/disable_functions = .*/disable_functions = /" /etc/php5/cli/php.ini

line_old='DocumentRoot /var/www/html'
line_new='DocumentRoot /var/www'
sed -i "s%$line_old%$line_new%g" /etc/apache2/sites-available/000-default.conf

sudo service apache2 restart

curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer


if [ -a /var/www/databases/latest.sql ]
	then
		mysql -u root -proot wordpress < /var/www/databases/latest.sql
	else
		mysql -uroot -proot -e "create database wordpress"
fi

(
	cd /usr/local/bin;
	curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar;
	sudo mv /usr/local/bin/wp-cli.phar /usr/local/bin/wp;
	sudo chmod +x /usr/local/bin/wp;
)
(
	cd /var/www;
	gem install yajl-ruby;
	gem install bundler;
	bundle install;
	bundle exec cap production wp:setup:local;
)