<?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>

    <div class="col-md-6">
        <?php the_content(); ?>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Button Styles</h3>
            </div>
            <div class="panel-body">
                <a class="btn btn-primary btn-lg btn-block">Blue Button</a>
                <a class="btn btn-success btn-lg btn-block">Green Button</a>
                <a class="btn btn-warning btn-lg btn-block">Burgundy Button</a>
                <a class="btn btn-danger btn-lg btn-block">Dark Button</a>
            </div>
        </div>
          <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Form Group Styles</h3>
            </div>
            <div class="panel-body">    
                <div class="input-group input-group-lg">
                    <input type="text" class="dark form-control" placeholder="email address">
                    <span class="input-group-btn">
                        <button class="btn btn-default btn-danger" type="button">Submit</button>
                    </span>
                </div>    
                <div class="input-group input-group-lg">
                    <input type="text" class="light form-control" placeholder="SEARCH ARTICLES">
                    <span class="input-group-btn">
                        <button class="btn btn-default btn-light" type="button">Submit</button>
                    </span>
                </div>
                <div class="btn-group btn-group-block">
                  <button type="button" class="btn btn-outline btn-lg">Dropdown</button>
                  <button type="button" class="btn btn-outline dropdown-toggle btn-lg" data-toggle="dropdown" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lightbox Button</h3>
            </div>
            <div class="panel-body">    
                <a class="btn btn-primary btn-lg btn-block lightbox-btn">Example Food picture</a>
            </div>
        </div>
