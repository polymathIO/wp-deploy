<header class="banner navbar navbar-default navbar-static-top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>">
        <span></span>
        <?php bloginfo('name'); ?>
        <span></span>
      </a>
    </div>

    <nav class="collapse navbar-collapse" role="navigation">
      <div class="social-widget-header">
        <a href="#" class="facebook"><img src="/content/themes/roots-sass/img/facebook.svg" height="25px"></a>
        <a href="#" class="twitter"><img src="/content/themes/roots-sass/img/twitter.svg" height="25px"></a>
        <a href="#" class="google"><img src="/content/themes/roots-sass/img/google.svg" height="25px"></a>
      </div>
      <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav'));
        endif;
      ?>
    </nav>
  </div>
  <nav class="secondary-nav">
    <div class="container">
      <a class="btn btn-success" href="#">Sign Up</a>
      <a class="btn btn-primary" href="#">Log In</a>

      <a href="#"><span class="glyphicon glyphicon-send" aria-hidden="true"></span>Contact</a>
      <a href="#"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>About</a>
      <a href="#"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Join Our Practitioner Network</a>
      <a href="#"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>Join Us</a>
    </div>
  </nav>
</header>
