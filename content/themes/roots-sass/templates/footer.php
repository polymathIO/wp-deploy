<footer class="content-info" role="contentinfo">
  <div class="container">
    <?php dynamic_sidebar('sidebar-footer'); ?>
  </div>
</footer>
<script src="//use.typekit.net/iqg7cyr.js"></script>
<script>try{Typekit.load();}catch(e){}</script>
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/js/scripts.min.js'></script>
<script src="//localhost:12346/livereload.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/slick/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/classList.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/tabby.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.accordion.js"></script>
<script type="text/javascript">
    $('.slides').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      autoplay: false,
      autoplaySpeed: 2000,
      arrows: true,
    });
	tabby.init();
    $( 'ul.accordion' ).accordion();

    $('.wide-bar').click(function(e){
    	var id = $(this).attr('id');
      $('.accordion-section.'+id).toggleClass('on');
    	$(this).toggleClass('on');
    });
</script>