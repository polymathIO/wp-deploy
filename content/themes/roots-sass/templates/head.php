<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php wp_title('|', true, 'right'); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel='stylesheet' id='roots_css-css' href='<?php echo get_template_directory_uri(); ?>/assets/css/main.css' type='text/css' media='all'/>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/slick/slick-theme.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/tabby.css"/>

	<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/js/vendor/modernizr.min.js'></script>
    <script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js'></script>
    <script>window.jQuery||document.write('<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/jquery/dist/jquery.js?1.11.1"><\/script>')</script>
  <?php wp_head(); ?>



  <link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('name'); ?> Feed" href="<?php echo esc_url(get_feed_link()); ?>">
</head>
