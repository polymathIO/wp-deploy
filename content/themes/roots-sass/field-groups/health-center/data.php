<?php 
$group = array (
  'id' => '54f4ad8e61a41',
  'title' => 'Health Center',
  'fields' => 
  array (
    0 => 
    array (
      'key' => 'field_54f4c107cd2c2',
      'label' => 'Hero Image',
      'name' => 'post-image',
      '_name' => 'post-image',
      'type' => 'image',
      'order_no' => 0,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-post-image',
      'class' => 'image',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'save_format' => 'url',
      'preview_size' => 'thumbnail',
      'library' => 'all',
      'field_group' => 66,
    ),
    1 => 
    array (
      'key' => 'field_54f4c0f890bc9',
      'label' => 'Overview',
      'name' => 'overview',
      '_name' => 'overview',
      'type' => 'wysiwyg',
      'order_no' => 1,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-overview',
      'class' => 'wysiwyg',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'default_value' => '',
      'toolbar' => 'full',
      'media_upload' => 'yes',
      'field_group' => 66,
    ),
    2 => 
    array (
      'key' => 'field_54f4db96d0406',
      'label' => 'Overview Image',
      'name' => 'overview_image',
      '_name' => 'overview_image',
      'type' => 'image',
      'order_no' => 2,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-overview_image',
      'class' => 'image',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
          ),
        ),
        'allorany' => 'all',
      ),
      'save_format' => 'url',
      'preview_size' => 'thumbnail',
      'library' => 'all',
      'field_group' => 66,
    ),
    3 => 
    array (
      'key' => 'field_54f4a2e9847a2',
      'label' => 'Articles',
      'name' => 'articles',
      '_name' => 'articles',
      'type' => 'relationship',
      'order_no' => 3,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-articles',
      'class' => 'relationship',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'return_format' => 'object',
      'post_type' => 
      array (
        0 => 'post',
      ),
      'taxonomy' => 
      array (
        0 => 'all',
      ),
      'filters' => 
      array (
        0 => 'search',
      ),
      'result_elements' => 
      array (
        0 => 'post_type',
        1 => 'post_title',
      ),
      'max' => '',
      'field_group' => 66,
    ),
    4 => 
    array (
      'key' => 'field_54f4a4aa7d15f',
      'label' => 'Characteristics',
      'name' => 'characteristics',
      '_name' => 'characteristics',
      'type' => 'relationship',
      'order_no' => 4,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-characteristics',
      'class' => 'relationship',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'return_format' => 'object',
      'post_type' => 
      array (
        0 => 'characteristic',
      ),
      'taxonomy' => 
      array (
        0 => 'all',
      ),
      'filters' => 
      array (
        0 => 'search',
      ),
      'result_elements' => 
      array (
        0 => 'post_type',
        1 => 'post_title',
      ),
      'max' => '',
      'field_group' => 66,
    ),
    5 => 
    array (
      'key' => 'field_54f4bfffade1e',
      'label' => 'Treatments',
      'name' => 'treatments',
      '_name' => 'treatments',
      'type' => 'relationship',
      'order_no' => 5,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-treatments',
      'class' => 'relationship',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'return_format' => 'object',
      'post_type' => 
      array (
        0 => 'treatment',
      ),
      'taxonomy' => 
      array (
        0 => 'all',
      ),
      'filters' => 
      array (
        0 => 'search',
      ),
      'result_elements' => 
      array (
        0 => 'post_type',
        1 => 'post_title',
      ),
      'max' => '',
      'field_group' => 66,
    ),
    6 => 
    array (
      'key' => 'field_54f4c01ac41dc',
      'label' => 'Supplements',
      'name' => 'supplements',
      '_name' => 'supplements',
      'type' => 'relationship',
      'order_no' => 6,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-supplements',
      'class' => 'relationship',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'return_format' => 'object',
      'post_type' => 
      array (
        0 => 'supplement',
      ),
      'taxonomy' => 
      array (
        0 => 'all',
      ),
      'filters' => 
      array (
        0 => 'search',
      ),
      'result_elements' => 
      array (
        0 => 'post_type',
        1 => 'post_title',
      ),
      'max' => '',
      'field_group' => 66,
    ),
    7 => 
    array (
      'key' => 'field_54f4a70538add',
      'label' => 'Home Tests',
      'name' => 'home_tests',
      '_name' => 'home_tests',
      'type' => 'relationship',
      'order_no' => 7,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-home_tests',
      'class' => 'relationship',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'return_format' => 'object',
      'post_type' => 
      array (
        0 => 'test',
      ),
      'taxonomy' => 
      array (
        0 => 'all',
      ),
      'filters' => 
      array (
        0 => 'search',
      ),
      'result_elements' => 
      array (
        0 => 'post_type',
        1 => 'post_title',
      ),
      'max' => '',
      'field_group' => 66,
    ),
    8 => 
    array (
      'key' => 'field_54f4ac087ec03',
      'label' => 'Sidebar Banner Images',
      'name' => 'sidebar_banner_images',
      '_name' => 'sidebar_banner_images',
      'type' => 'repeater',
      'order_no' => 8,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-sidebar_banner_images',
      'class' => 'repeater',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'sub_fields' => 
      array (
        0 => 
        array (
          'key' => 'field_54f4ac237ec04',
          'label' => 'Banner Image',
          'name' => 'banner_image',
          '_name' => 'banner_image',
          'type' => 'image',
          'order_no' => 0,
          'instructions' => '',
          'required' => 0,
          'id' => 'acf-field-banner_image',
          'class' => 'image',
          'conditional_logic' => 
          array (
            'status' => 0,
            'rules' => 
            array (
              0 => 
              array (
                'field' => 'null',
                'operator' => '==',
                'value' => '',
              ),
            ),
            'allorany' => 'all',
          ),
          'column_width' => '',
          'save_format' => 'object',
          'preview_size' => 'thumbnail',
          'library' => 'all',
        ),
        1 => 
        array (
          'key' => 'field_54f4ac647ec05',
          'label' => 'Banner URL',
          'name' => 'banner_url',
          '_name' => 'banner_url',
          'type' => 'text',
          'order_no' => 1,
          'instructions' => '',
          'required' => 0,
          'id' => 'acf-field-banner_url',
          'class' => 'text',
          'conditional_logic' => 
          array (
            'status' => 0,
            'rules' => 
            array (
              0 => 
              array (
                'field' => 'null',
                'operator' => '==',
                'value' => '',
              ),
            ),
            'allorany' => 'all',
          ),
          'column_width' => '',
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'formatting' => 'html',
          'maxlength' => '',
        ),
      ),
      'row_min' => '',
      'row_limit' => '',
      'layout' => 'table',
      'button_label' => 'Add Row',
      'field_group' => 66,
    ),
  ),
  'location' => 
  array (
    0 => 
    array (
      0 => 
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'health-center',
        'order_no' => 0,
        'group_no' => 0,
      ),
    ),
  ),
  'options' => 
  array (
    'position' => 'normal',
    'layout' => 'no_box',
    'hide_on_screen' => 
    array (
    ),
  ),
  'menu_order' => 0,
);