<?php 
$group = array (
  'id' => '54ea807cc17b8',
  'title' => 'Tests',
  'fields' => 
  array (
    0 => 
    array (
      'key' => 'field_54ea7c94d8851',
      'label' => 'Test Type',
      'name' => 'test_type',
      '_name' => 'test_type',
      'type' => 'select',
      'order_no' => 0,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-test_type',
      'class' => 'select',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
          ),
        ),
        'allorany' => 'all',
      ),
      'choices' => 
      array (
        'home' => 'Home Test',
        'lab' => 'Lab Test',
      ),
      'default_value' => '',
      'allow_null' => 0,
      'multiple' => 0,
      'field_group' => 16,
    ),
    1 => 
    array (
      'key' => 'field_54ea7ddb123cd',
      'label' => 'Image',
      'name' => 'image',
      '_name' => 'image',
      'type' => 'image',
      'order_no' => 1,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-image',
      'class' => 'image',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'field_54ea7c94d8851',
            'operator' => '==',
            'value' => 'Home Test',
          ),
        ),
        'allorany' => 'all',
      ),
      'save_format' => 'url',
      'preview_size' => 'thumbnail',
      'library' => 'all',
      'field_group' => 16,
    ),
  ),
  'location' => 
  array (
    0 => 
    array (
      0 => 
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'test',
        'order_no' => 0,
        'group_no' => 0,
      ),
    ),
  ),
  'options' => 
  array (
    'position' => 'normal',
    'layout' => 'no_box',
    'hide_on_screen' => 
    array (
      0 => 'excerpt',
      1 => 'custom_fields',
      2 => 'discussion',
      3 => 'comments',
      4 => 'revisions',
      5 => 'author',
      6 => 'format',
      7 => 'categories',
      8 => 'tags',
      9 => 'send-trackbacks',
    ),
  ),
  'menu_order' => 0,
);