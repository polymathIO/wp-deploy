<?php 
$group = array (
  'id' => '54f4dd8edf0b3',
  'title' => 'Characteristics',
  'fields' => 
  array (
    0 => 
    array (
      'key' => 'field_54f4bbebb64e0',
      'label' => 'Image',
      'name' => 'image',
      '_name' => 'image',
      'type' => 'image',
      'order_no' => 0,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-image',
      'class' => 'image',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
          ),
        ),
        'allorany' => 'all',
      ),
      'save_format' => 'object',
      'preview_size' => 'thumbnail',
      'library' => 'all',
      'field_group' => 67,
    ),
  ),
  'location' => 
  array (
    0 => 
    array (
      0 => 
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'characteristic',
        'order_no' => 0,
        'group_no' => 0,
      ),
    ),
    1 => 
    array (
      0 => 
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'supplement',
        'order_no' => 0,
        'group_no' => 1,
      ),
    ),
    2 => 
    array (
      0 => 
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'treatment',
        'order_no' => 0,
        'group_no' => 2,
      ),
    ),
  ),
  'options' => 
  array (
    'position' => 'normal',
    'layout' => 'no_box',
    'hide_on_screen' => 
    array (
    ),
  ),
  'menu_order' => 0,
);