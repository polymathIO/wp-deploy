<?php 
$group = array (
  'id' => '54ea6a6628591',
  'title' => 'Videos',
  'fields' => 
  array (
    0 => 
    array (
      'key' => 'field_54e60a574b52d',
      'label' => 'Video URL',
      'name' => 'video_url',
      '_name' => 'video_url',
      'type' => 'text',
      'order_no' => 0,
      'instructions' => 'ex: https://www.youtube.com/watch?v=xUdks1RtlIk',
      'required' => 1,
      'id' => 'acf-field-video_url',
      'class' => 'text',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'formatting' => 'none',
      'maxlength' => '',
      'field_group' => 4,
    ),
    1 => 
    array (
      'key' => 'field_54e60a93f22fa',
      'label' => 'Video Source',
      'name' => 'video_source',
      '_name' => 'video_source',
      'type' => 'select',
      'order_no' => 1,
      'instructions' => '',
      'required' => 1,
      'id' => 'acf-field-video_source',
      'class' => 'select',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
          ),
        ),
        'allorany' => 'all',
      ),
      'choices' => 
      array (
        'vimeo' => 'Vimeo',
        'youtube' => 'Youtube',
        'generic' => 'Generic Embed Code',
      ),
      'default_value' => '',
      'allow_null' => 0,
      'multiple' => 0,
      'field_group' => 4,
    ),
  ),
  'location' => 
  array (
    0 => 
    array (
      0 => 
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'video',
        'order_no' => 0,
        'group_no' => 0,
      ),
    ),
  ),
  'options' => 
  array (
    'position' => 'normal',
    'layout' => 'no_box',
    'hide_on_screen' => 
    array (
      0 => 'the_content',
      1 => 'excerpt',
      2 => 'custom_fields',
      3 => 'discussion',
      4 => 'comments',
      5 => 'revisions',
      6 => 'author',
      7 => 'format',
      8 => 'featured_image',
      9 => 'categories',
      10 => 'tags',
      11 => 'send-trackbacks',
    ),
  ),
  'menu_order' => 0,
);