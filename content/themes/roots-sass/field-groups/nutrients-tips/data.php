<?php 
$group = array (
  'id' => '54ea807cc2d02',
  'title' => 'Nutrients, Tips',
  'fields' => 
  array (
    0 => 
    array (
      'key' => 'field_54ea804b57e2f',
      'label' => 'Image',
      'name' => 'image',
      '_name' => 'image',
      'type' => 'image',
      'order_no' => 0,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-image',
      'class' => 'image',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'save_format' => 'object',
      'preview_size' => 'thumbnail',
      'library' => 'all',
      'field_group' => 19,
    ),
  ),
  'location' => 
  array (
    0 => 
    array (
      0 => 
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'tip',
        'order_no' => 0,
        'group_no' => 0,
      ),
    ),
    1 => 
    array (
      0 => 
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'nutrient',
        'order_no' => 0,
        'group_no' => 1,
      ),
    ),
  ),
  'options' => 
  array (
    'position' => 'normal',
    'layout' => 'no_box',
    'hide_on_screen' => 
    array (
    ),
  ),
  'menu_order' => 0,
);