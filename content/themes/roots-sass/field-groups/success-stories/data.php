<?php 
$group = array (
  'id' => '54ea6a662aa8d',
  'title' => 'Success Stories',
  'fields' => 
  array (
    0 => 
    array (
      'key' => 'field_54e60b93a3ba2',
      'label' => 'Video',
      'name' => 'video',
      '_name' => 'video',
      'type' => 'relationship',
      'order_no' => 0,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-video',
      'class' => 'relationship',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'return_format' => 'object',
      'post_type' => 
      array (
        0 => 'video',
      ),
      'taxonomy' => 
      array (
        0 => 'all',
      ),
      'filters' => 
      array (
        0 => 'search',
      ),
      'result_elements' => 
      array (
        0 => 'post_type',
        1 => 'post_title',
      ),
      'max' => 1,
      'field_group' => 8,
    ),
  ),
  'location' => 
  array (
    0 => 
    array (
      0 => 
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'success-story',
        'order_no' => 0,
        'group_no' => 0,
      ),
    ),
  ),
  'options' => 
  array (
    'position' => 'acf_after_title',
    'layout' => 'no_box',
    'hide_on_screen' => 
    array (
      0 => 'excerpt',
      1 => 'custom_fields',
      2 => 'discussion',
      3 => 'comments',
      4 => 'revisions',
      5 => 'author',
      6 => 'format',
      7 => 'featured_image',
      8 => 'categories',
      9 => 'tags',
      10 => 'send-trackbacks',
    ),
  ),
  'menu_order' => 0,
);