<?php 
$group = array (
  'id' => '54f08b45a9c5e',
  'title' => 'Health Conditions',
  'fields' => 
  array (
    0 => 
    array (
      'key' => 'field_54f1857718cd1',
      'label' => 'Post Image',
      'name' => 'post-image',
      '_name' => 'post-image',
      'type' => 'image',
      'order_no' => 0,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-post-image',
      'class' => 'image',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
          ),
        ),
        'allorany' => 'all',
      ),
      'save_format' => 'url',
      'preview_size' => 'thumbnail',
      'library' => 'all',
      'field_group' => 24,
    ),
    1 => 
    array (
      'key' => 'field_54f0cc5b18e4d',
      'label' => 'What is "Condition"',
      'name' => 'cond_description',
      '_name' => 'cond_description',
      'type' => 'wysiwyg',
      'order_no' => 1,
      'instructions' => '',
      'required' => 1,
      'id' => 'acf-field-cond_description',
      'class' => 'wysiwyg',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'default_value' => '',
      'toolbar' => 'full',
      'media_upload' => 'yes',
      'field_group' => 24,
    ),
    2 => 
    array (
      'key' => 'field_54ea83bcc836b',
      'label' => 'Symptoms',
      'name' => 'symptoms',
      '_name' => 'symptoms',
      'type' => 'repeater',
      'order_no' => 2,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-symptoms',
      'class' => 'repeater',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'sub_fields' => 
      array (
        0 => 
        array (
          'key' => 'field_54f0d6522220d',
          'label' => 'Title',
          'name' => 'title',
          '_name' => 'title',
          'type' => 'text',
          'order_no' => 0,
          'instructions' => '',
          'required' => 0,
          'id' => 'acf-field-title',
          'class' => 'text',
          'conditional_logic' => 
          array (
            'status' => 0,
            'rules' => 
            array (
              0 => 
              array (
                'field' => 'null',
                'operator' => '==',
                'value' => '',
              ),
            ),
            'allorany' => 'all',
          ),
          'column_width' => '',
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'formatting' => 'html',
          'maxlength' => '',
        ),
        1 => 
        array (
          'key' => 'field_54f0d6572220e',
          'label' => 'Description',
          'name' => 'description',
          '_name' => 'description',
          'type' => 'textarea',
          'order_no' => 1,
          'instructions' => '',
          'required' => 0,
          'id' => 'acf-field-description',
          'class' => 'textarea',
          'conditional_logic' => 
          array (
            'status' => 0,
            'rules' => 
            array (
              0 => 
              array (
                'field' => 'null',
                'operator' => '==',
                'value' => '',
              ),
            ),
            'allorany' => 'all',
          ),
          'column_width' => '',
          'default_value' => '',
          'placeholder' => '',
          'maxlength' => '',
          'rows' => '',
          'formatting' => 'br',
        ),
        2 => 
        array (
          'key' => 'field_54f0d65e2220f',
          'label' => 'Image',
          'name' => 'image',
          '_name' => 'image',
          'type' => 'image',
          'order_no' => 2,
          'instructions' => '',
          'required' => 0,
          'id' => 'acf-field-image',
          'class' => 'image',
          'conditional_logic' => 
          array (
            'status' => 0,
            'rules' => 
            array (
              0 => 
              array (
                'field' => 'null',
                'operator' => '==',
                'value' => '',
              ),
            ),
            'allorany' => 'all',
          ),
          'column_width' => '',
          'save_format' => 'object',
          'preview_size' => 'thumbnail',
          'library' => 'all',
        ),
      ),
      'row_min' => '',
      'row_limit' => '',
      'layout' => 'table',
      'button_label' => 'Add Row',
      'field_group' => 24,
    ),
    3 => 
    array (
      'key' => 'field_54ea84046ef23',
      'label' => 'Causes',
      'name' => 'causes',
      '_name' => 'causes',
      'type' => 'repeater',
      'order_no' => 3,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-causes',
      'class' => 'repeater',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'sub_fields' => 
      array (
        0 => 
        array (
          'key' => 'field_54ea84046ef24',
          'label' => 'Title',
          'name' => 'title',
          '_name' => 'title',
          'type' => 'text',
          'order_no' => 0,
          'instructions' => '',
          'required' => 0,
          'id' => 'acf-field-title',
          'class' => 'text',
          'conditional_logic' => 
          array (
            'status' => 0,
            'rules' => 
            array (
              0 => 
              array (
                'field' => 'null',
                'operator' => '==',
                'value' => '',
              ),
            ),
            'allorany' => 'all',
          ),
          'column_width' => '',
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'formatting' => 'html',
          'maxlength' => '',
        ),
        1 => 
        array (
          'key' => 'field_54ea84046ef25',
          'label' => 'Description',
          'name' => 'description',
          '_name' => 'description',
          'type' => 'textarea',
          'order_no' => 1,
          'instructions' => '',
          'required' => 0,
          'id' => 'acf-field-description',
          'class' => 'textarea',
          'conditional_logic' => 
          array (
            'status' => 0,
            'rules' => 
            array (
              0 => 
              array (
                'field' => 'null',
                'operator' => '==',
                'value' => '',
              ),
            ),
            'allorany' => 'all',
          ),
          'column_width' => '',
          'default_value' => '',
          'placeholder' => '',
          'maxlength' => '',
          'rows' => '',
          'formatting' => 'br',
        ),
        2 => 
        array (
          'key' => 'field_54ea84046ef26',
          'label' => 'Image',
          'name' => 'image',
          '_name' => 'image',
          'type' => 'image',
          'order_no' => 2,
          'instructions' => '',
          'required' => 0,
          'id' => 'acf-field-image',
          'class' => 'image',
          'conditional_logic' => 
          array (
            'status' => 0,
            'rules' => 
            array (
              0 => 
              array (
                'field' => 'null',
                'operator' => '==',
                'value' => '',
              ),
            ),
            'allorany' => 'all',
          ),
          'column_width' => '',
          'save_format' => 'object',
          'preview_size' => 'thumbnail',
          'library' => 'all',
        ),
      ),
      'row_min' => '',
      'row_limit' => '',
      'layout' => 'table',
      'button_label' => 'Add Row',
      'field_group' => 24,
    ),
    4 => 
    array (
      'key' => 'field_54ea85a0f4389',
      'label' => 'Quiz',
      'name' => 'quiz',
      '_name' => 'quiz',
      'type' => 'relationship',
      'order_no' => 4,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-quiz',
      'class' => 'relationship',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'return_format' => 'object',
      'post_type' => 
      array (
        0 => 'quiz',
      ),
      'taxonomy' => 
      array (
        0 => 'all',
      ),
      'filters' => 
      array (
        0 => 'search',
      ),
      'result_elements' => 
      array (
        0 => 'post_type',
        1 => 'post_title',
      ),
      'max' => 1,
      'field_group' => 24,
    ),
    5 => 
    array (
      'key' => 'field_54ea855d5b323',
      'label' => 'Myths',
      'name' => 'myths',
      '_name' => 'myths',
      'type' => 'repeater',
      'order_no' => 5,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-myths',
      'class' => 'repeater',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'sub_fields' => 
      array (
        0 => 
        array (
          'key' => 'field_54ea85875b324',
          'label' => 'Myth',
          'name' => 'myth',
          '_name' => 'myth',
          'type' => 'text',
          'order_no' => 0,
          'instructions' => '',
          'required' => 0,
          'id' => 'acf-field-myth',
          'class' => 'text',
          'conditional_logic' => 
          array (
            'status' => 0,
            'rules' => 
            array (
              0 => 
              array (
                'field' => 'null',
                'operator' => '==',
                'value' => '',
              ),
            ),
            'allorany' => 'all',
          ),
          'column_width' => '',
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'formatting' => 'html',
          'maxlength' => '',
        ),
        1 => 
        array (
          'key' => 'field_54ea858d5b325',
          'label' => 'Reality',
          'name' => 'reality',
          '_name' => 'reality',
          'type' => 'textarea',
          'order_no' => 1,
          'instructions' => '',
          'required' => 0,
          'id' => 'acf-field-reality',
          'class' => 'textarea',
          'conditional_logic' => 
          array (
            'status' => 0,
            'rules' => 
            array (
              0 => 
              array (
                'field' => 'null',
                'operator' => '==',
                'value' => '',
              ),
            ),
            'allorany' => 'all',
          ),
          'column_width' => '',
          'default_value' => '',
          'placeholder' => '',
          'maxlength' => '',
          'rows' => '',
          'formatting' => 'br',
        ),
      ),
      'row_min' => '',
      'row_limit' => '',
      'layout' => 'table',
      'button_label' => 'Add Row',
      'field_group' => 24,
    ),
    6 => 
    array (
      'key' => 'field_54f162729ac7d',
      'label' => 'Tests',
      'name' => 'tests',
      '_name' => 'tests',
      'type' => 'relationship',
      'order_no' => 6,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-tests',
      'class' => 'relationship',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'return_format' => 'object',
      'post_type' => 
      array (
        0 => 'test',
      ),
      'taxonomy' => 
      array (
        0 => 'all',
      ),
      'filters' => 
      array (
        0 => 'search',
      ),
      'result_elements' => 
      array (
        0 => 'post_type',
        1 => 'post_title',
      ),
      'max' => '',
      'field_group' => 24,
    ),
    7 => 
    array (
      'key' => 'field_54ea8418f456d',
      'label' => 'Nutrients',
      'name' => '',
      '_name' => '',
      'type' => 'tab',
      'order_no' => 7,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-',
      'class' => 'tab',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'field_group' => 24,
    ),
    8 => 
    array (
      'key' => 'field_54ea844cf456e',
      'label' => 'Nutrients',
      'name' => 'nutrients',
      '_name' => 'nutrients',
      'type' => 'relationship',
      'order_no' => 8,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-nutrients',
      'class' => 'relationship',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'return_format' => 'object',
      'post_type' => 
      array (
        0 => 'nutrient',
      ),
      'taxonomy' => 
      array (
        0 => 'all',
      ),
      'filters' => 
      array (
        0 => 'search',
      ),
      'result_elements' => 
      array (
        0 => 'post_type',
        1 => 'post_title',
      ),
      'max' => '',
      'field_group' => 24,
    ),
    9 => 
    array (
      'key' => 'field_54ea8473be528',
      'label' => 'Approach',
      'name' => '',
      '_name' => '',
      'type' => 'tab',
      'order_no' => 9,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-',
      'class' => 'tab',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'field_group' => 24,
    ),
    10 => 
    array (
      'key' => 'field_54ea847fbe529',
      'label' => 'Title',
      'name' => 'approach_title',
      '_name' => 'approach_title',
      'type' => 'text',
      'order_no' => 10,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-approach_title',
      'class' => 'text',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'formatting' => 'html',
      'maxlength' => '',
      'field_group' => 24,
    ),
    11 => 
    array (
      'key' => 'field_54ea8486be52a',
      'label' => 'Description',
      'name' => 'approach_description',
      '_name' => 'approach_description',
      'type' => 'wysiwyg',
      'order_no' => 11,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-approach_description',
      'class' => 'wysiwyg',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'default_value' => '',
      'toolbar' => 'full',
      'media_upload' => 'yes',
      'field_group' => 24,
    ),
    12 => 
    array (
      'key' => 'field_54ea8494be52b',
      'label' => 'Image',
      'name' => 'image',
      '_name' => 'image',
      'type' => 'image',
      'order_no' => 12,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-image',
      'class' => 'image',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'save_format' => 'object',
      'preview_size' => 'thumbnail',
      'library' => 'all',
      'field_group' => 24,
    ),
    13 => 
    array (
      'key' => 'field_54ea84a3be52c',
      'label' => 'Tips',
      'name' => '',
      '_name' => '',
      'type' => 'tab',
      'order_no' => 13,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-',
      'class' => 'tab',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'field_group' => 24,
    ),
    14 => 
    array (
      'key' => 'field_54ea84acbe52d',
      'label' => 'Tips',
      'name' => 'tips',
      '_name' => 'tips',
      'type' => 'relationship',
      'order_no' => 14,
      'instructions' => '',
      'required' => 0,
      'id' => 'acf-field-tips',
      'class' => 'relationship',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'return_format' => 'object',
      'post_type' => 
      array (
        0 => 'tip',
      ),
      'taxonomy' => 
      array (
        0 => 'all',
      ),
      'filters' => 
      array (
        0 => 'search',
      ),
      'result_elements' => 
      array (
        0 => 'post_type',
        1 => 'post_title',
      ),
      'max' => '',
      'field_group' => 24,
    ),
  ),
  'location' => 
  array (
    0 => 
    array (
      0 => 
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'condition',
        'order_no' => 0,
        'group_no' => 0,
      ),
    ),
  ),
  'options' => 
  array (
    'position' => 'normal',
    'layout' => 'no_box',
    'hide_on_screen' => 
    array (
    ),
  ),
  'menu_order' => 0,
);