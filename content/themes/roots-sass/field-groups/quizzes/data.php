<?php 
$group = array (
  'id' => '54ea807cbf8de',
  'title' => 'Quizzes',
  'fields' => 
  array (
    0 => 
    array (
      'key' => 'field_54ea782820231',
      'label' => 'Question',
      'name' => 'question',
      '_name' => 'question',
      'type' => 'repeater',
      'order_no' => 0,
      'instructions' => '',
      'required' => 1,
      'id' => 'acf-field-question',
      'class' => 'repeater',
      'conditional_logic' => 
      array (
        'status' => 0,
        'rules' => 
        array (
          0 => 
          array (
            'field' => 'null',
            'operator' => '==',
            'value' => '',
          ),
        ),
        'allorany' => 'all',
      ),
      'sub_fields' => 
      array (
        0 => 
        array (
          'key' => 'field_54ea789b20232',
          'label' => 'Question',
          'name' => 'question',
          '_name' => 'question',
          'type' => 'text',
          'order_no' => 0,
          'instructions' => '',
          'required' => 1,
          'id' => 'acf-field-question',
          'class' => 'text',
          'conditional_logic' => 
          array (
            'status' => 0,
            'rules' => 
            array (
              0 => 
              array (
                'field' => 'null',
                'operator' => '==',
                'value' => '',
              ),
            ),
            'allorany' => 'all',
          ),
          'column_width' => '',
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'formatting' => 'html',
          'maxlength' => '',
        ),
        1 => 
        array (
          'key' => 'field_54ea78b620233',
          'label' => 'Answer',
          'name' => 'answer',
          '_name' => 'answer',
          'type' => 'repeater',
          'order_no' => 1,
          'instructions' => '',
          'required' => 0,
          'id' => 'acf-field-answer',
          'class' => 'repeater',
          'conditional_logic' => 
          array (
            'status' => 0,
            'rules' => 
            array (
              0 => 
              array (
                'field' => 'null',
                'operator' => '==',
                'value' => '',
              ),
            ),
            'allorany' => 'all',
          ),
          'column_width' => '',
          'sub_fields' => 
          array (
            0 => 
            array (
              'key' => 'field_54ea78cf20234',
              'label' => 'Answer Text',
              'name' => 'answer_text',
              '_name' => 'answer_text',
              'type' => 'text',
              'order_no' => 0,
              'instructions' => '',
              'required' => 0,
              'id' => 'acf-field-answer_text',
              'class' => 'text',
              'conditional_logic' => 
              array (
                'status' => 0,
                'rules' => 
                array (
                  0 => 
                  array (
                    'field' => 'null',
                    'operator' => '==',
                    'value' => '',
                  ),
                ),
                'allorany' => 'all',
              ),
              'column_width' => '',
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => '',
              'formatting' => 'html',
              'maxlength' => '',
            ),
            1 => 
            array (
              'key' => 'field_54ea78d820235',
              'label' => 'Correct?',
              'name' => 'correct',
              '_name' => 'correct',
              'type' => 'true_false',
              'order_no' => 1,
              'instructions' => '',
              'required' => 0,
              'id' => 'acf-field-correct',
              'class' => 'true_false',
              'conditional_logic' => 
              array (
                'status' => 0,
                'rules' => 
                array (
                  0 => 
                  array (
                    'field' => 'null',
                    'operator' => '==',
                    'value' => '',
                  ),
                ),
                'allorany' => 'all',
              ),
              'column_width' => '',
              'message' => '',
              'default_value' => 0,
            ),
          ),
          'row_min' => '',
          'row_limit' => '',
          'layout' => 'table',
          'button_label' => 'Add Row',
        ),
      ),
      'row_min' => 1,
      'row_limit' => '',
      'layout' => 'table',
      'button_label' => 'Add Row',
      'field_group' => 14,
    ),
  ),
  'location' => 
  array (
    0 => 
    array (
      0 => 
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'quiz',
        'order_no' => 0,
        'group_no' => 0,
      ),
    ),
  ),
  'options' => 
  array (
    'position' => 'normal',
    'layout' => 'no_box',
    'hide_on_screen' => 
    array (
      0 => 'the_content',
      1 => 'excerpt',
      2 => 'custom_fields',
      3 => 'discussion',
      4 => 'comments',
      5 => 'revisions',
      6 => 'author',
      7 => 'format',
      8 => 'categories',
      9 => 'tags',
      10 => 'send-trackbacks',
    ),
  ),
  'menu_order' => 0,
);