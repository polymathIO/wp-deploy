<?php

function treatment_init() {
	register_post_type( 'treatment', array(
		'labels'            => array(
			'name'                => __( 'Treatments', 'roots-sass' ),
			'singular_name'       => __( 'Treatment', 'roots-sass' ),
			'all_items'           => __( 'Treatments', 'roots-sass' ),
			'new_item'            => __( 'New treatment', 'roots-sass' ),
			'add_new'             => __( 'Add New', 'roots-sass' ),
			'add_new_item'        => __( 'Add New treatment', 'roots-sass' ),
			'edit_item'           => __( 'Edit treatment', 'roots-sass' ),
			'view_item'           => __( 'View treatment', 'roots-sass' ),
			'search_items'        => __( 'Search treatments', 'roots-sass' ),
			'not_found'           => __( 'No treatments found', 'roots-sass' ),
			'not_found_in_trash'  => __( 'No treatments found in trash', 'roots-sass' ),
			'parent_item_colon'   => __( 'Parent treatment', 'roots-sass' ),
			'menu_name'           => __( 'Treatments', 'roots-sass' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
	) );

}
add_action( 'init', 'treatment_init' );

function treatment_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['treatment'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Treatment updated. <a target="_blank" href="%s">View treatment</a>', 'roots-sass'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'roots-sass'),
		3 => __('Custom field deleted.', 'roots-sass'),
		4 => __('Treatment updated.', 'roots-sass'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Treatment restored to revision from %s', 'roots-sass'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Treatment published. <a href="%s">View treatment</a>', 'roots-sass'), esc_url( $permalink ) ),
		7 => __('Treatment saved.', 'roots-sass'),
		8 => sprintf( __('Treatment submitted. <a target="_blank" href="%s">Preview treatment</a>', 'roots-sass'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Treatment scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview treatment</a>', 'roots-sass'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Treatment draft updated. <a target="_blank" href="%s">Preview treatment</a>', 'roots-sass'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'treatment_updated_messages' );
