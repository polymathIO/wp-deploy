<?php

function test_init() {
	register_post_type( 'test', array(
		'labels'            => array(
			'name'                => __( 'Tests', 'roots-sass' ),
			'singular_name'       => __( 'Test', 'roots-sass' ),
			'all_items'           => __( 'Tests', 'roots-sass' ),
			'new_item'            => __( 'New test', 'roots-sass' ),
			'add_new'             => __( 'Add New', 'roots-sass' ),
			'add_new_item'        => __( 'Add New test', 'roots-sass' ),
			'edit_item'           => __( 'Edit test', 'roots-sass' ),
			'view_item'           => __( 'View test', 'roots-sass' ),
			'search_items'        => __( 'Search tests', 'roots-sass' ),
			'not_found'           => __( 'No tests found', 'roots-sass' ),
			'not_found_in_trash'  => __( 'No tests found in trash', 'roots-sass' ),
			'parent_item_colon'   => __( 'Parent test', 'roots-sass' ),
			'menu_name'           => __( 'Tests', 'roots-sass' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
	) );

}
add_action( 'init', 'test_init' );

function test_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['test'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Test updated. <a target="_blank" href="%s">View test</a>', 'roots-sass'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'roots-sass'),
		3 => __('Custom field deleted.', 'roots-sass'),
		4 => __('Test updated.', 'roots-sass'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Test restored to revision from %s', 'roots-sass'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Test published. <a href="%s">View test</a>', 'roots-sass'), esc_url( $permalink ) ),
		7 => __('Test saved.', 'roots-sass'),
		8 => sprintf( __('Test submitted. <a target="_blank" href="%s">Preview test</a>', 'roots-sass'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Test scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview test</a>', 'roots-sass'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Test draft updated. <a target="_blank" href="%s">Preview test</a>', 'roots-sass'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'test_updated_messages' );
