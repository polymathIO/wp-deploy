<?php

function nutrient_init() {
	register_post_type( 'nutrient', array(
		'labels'            => array(
			'name'                => __( 'Nutrients' ),
			'singular_name'       => __( 'Nutrient' ),
			'all_items'           => __( 'Nutrients' ),
			'new_item'            => __( 'New nutrient' ),
			'add_new'             => __( 'Add New' ),
			'add_new_item'        => __( 'Add New nutrient' ),
			'edit_item'           => __( 'Edit nutrient' ),
			'view_item'           => __( 'View nutrient' ),
			'search_items'        => __( 'Search nutrients' ),
			'not_found'           => __( 'No nutrients found' ),
			'not_found_in_trash'  => __( 'No nutrients found in trash' ),
			'parent_item_colon'   => __( 'Parent nutrient' ),
			'menu_name'           => __( 'Nutrients' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
	) );

}
add_action( 'init', 'nutrient_init' );

function nutrient_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['nutrient'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Nutrient updated. <a target="_blank" href="%s">View nutrient</a>'), esc_url( $permalink ) ),
		2 => __('Custom field updated.'),
		3 => __('Custom field deleted.'),
		4 => __('Nutrient updated.'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Nutrient restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Nutrient published. <a href="%s">View nutrient</a>'), esc_url( $permalink ) ),
		7 => __('Nutrient saved.'),
		8 => sprintf( __('Nutrient submitted. <a target="_blank" href="%s">Preview nutrient</a>'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Nutrient scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview nutrient</a>'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Nutrient draft updated. <a target="_blank" href="%s">Preview nutrient</a>'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'nutrient_updated_messages' );
