<?php

function supplement_init() {
	register_post_type( 'supplement', array(
		'labels'            => array(
			'name'                => __( 'Supplements', 'roots-sass' ),
			'singular_name'       => __( 'Supplement', 'roots-sass' ),
			'all_items'           => __( 'Supplements', 'roots-sass' ),
			'new_item'            => __( 'New supplement', 'roots-sass' ),
			'add_new'             => __( 'Add New', 'roots-sass' ),
			'add_new_item'        => __( 'Add New supplement', 'roots-sass' ),
			'edit_item'           => __( 'Edit supplement', 'roots-sass' ),
			'view_item'           => __( 'View supplement', 'roots-sass' ),
			'search_items'        => __( 'Search supplements', 'roots-sass' ),
			'not_found'           => __( 'No supplements found', 'roots-sass' ),
			'not_found_in_trash'  => __( 'No supplements found in trash', 'roots-sass' ),
			'parent_item_colon'   => __( 'Parent supplement', 'roots-sass' ),
			'menu_name'           => __( 'Supplements', 'roots-sass' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
	) );

}
add_action( 'init', 'supplement_init' );

function supplement_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['supplement'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Supplement updated. <a target="_blank" href="%s">View supplement</a>', 'roots-sass'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'roots-sass'),
		3 => __('Custom field deleted.', 'roots-sass'),
		4 => __('Supplement updated.', 'roots-sass'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Supplement restored to revision from %s', 'roots-sass'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Supplement published. <a href="%s">View supplement</a>', 'roots-sass'), esc_url( $permalink ) ),
		7 => __('Supplement saved.', 'roots-sass'),
		8 => sprintf( __('Supplement submitted. <a target="_blank" href="%s">Preview supplement</a>', 'roots-sass'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Supplement scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview supplement</a>', 'roots-sass'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Supplement draft updated. <a target="_blank" href="%s">Preview supplement</a>', 'roots-sass'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'supplement_updated_messages' );
