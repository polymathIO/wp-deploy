<?php

function condition_init() {
	register_post_type( 'condition', array(
		'labels'            => array(
			'name'                => __( 'Conditions', 'roots-sass' ),
			'singular_name'       => __( 'Condition', 'roots-sass' ),
			'all_items'           => __( 'Conditions', 'roots-sass' ),
			'new_item'            => __( 'New condition', 'roots-sass' ),
			'add_new'             => __( 'Add New', 'roots-sass' ),
			'add_new_item'        => __( 'Add New condition', 'roots-sass' ),
			'edit_item'           => __( 'Edit condition', 'roots-sass' ),
			'view_item'           => __( 'View condition', 'roots-sass' ),
			'search_items'        => __( 'Search conditions', 'roots-sass' ),
			'not_found'           => __( 'No conditions found', 'roots-sass' ),
			'not_found_in_trash'  => __( 'No conditions found in trash', 'roots-sass' ),
			'parent_item_colon'   => __( 'Parent condition', 'roots-sass' ),
			'menu_name'           => __( 'Conditions', 'roots-sass' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
	) );

}
add_action( 'init', 'condition_init' );

function condition_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['condition'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Condition updated. <a target="_blank" href="%s">View condition</a>', 'roots-sass'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'roots-sass'),
		3 => __('Custom field deleted.', 'roots-sass'),
		4 => __('Condition updated.', 'roots-sass'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Condition restored to revision from %s', 'roots-sass'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Condition published. <a href="%s">View condition</a>', 'roots-sass'), esc_url( $permalink ) ),
		7 => __('Condition saved.', 'roots-sass'),
		8 => sprintf( __('Condition submitted. <a target="_blank" href="%s">Preview condition</a>', 'roots-sass'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Condition scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview condition</a>', 'roots-sass'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Condition draft updated. <a target="_blank" href="%s">Preview condition</a>', 'roots-sass'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'condition_updated_messages' );
