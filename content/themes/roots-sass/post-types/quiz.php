<?php

function quiz_init() {
	register_post_type( 'quiz', array(
		'labels'            => array(
			'name'                => __( 'Quizzes', 'roots-sass' ),
			'singular_name'       => __( 'Quiz', 'roots-sass' ),
			'all_items'           => __( 'Quizzes', 'roots-sass' ),
			'new_item'            => __( 'New quiz', 'roots-sass' ),
			'add_new'             => __( 'Add New', 'roots-sass' ),
			'add_new_item'        => __( 'Add New quiz', 'roots-sass' ),
			'edit_item'           => __( 'Edit quiz', 'roots-sass' ),
			'view_item'           => __( 'View quiz', 'roots-sass' ),
			'search_items'        => __( 'Search quizzes', 'roots-sass' ),
			'not_found'           => __( 'No quizzes found', 'roots-sass' ),
			'not_found_in_trash'  => __( 'No quizzes found in trash', 'roots-sass' ),
			'parent_item_colon'   => __( 'Parent quiz', 'roots-sass' ),
			'menu_name'           => __( 'Quizzes', 'roots-sass' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
	) );

}
add_action( 'init', 'quiz_init' );

function quiz_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['quiz'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Quiz updated. <a target="_blank" href="%s">View quiz</a>', 'roots-sass'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'roots-sass'),
		3 => __('Custom field deleted.', 'roots-sass'),
		4 => __('Quiz updated.', 'roots-sass'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Quiz restored to revision from %s', 'roots-sass'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Quiz published. <a href="%s">View quiz</a>', 'roots-sass'), esc_url( $permalink ) ),
		7 => __('Quiz saved.', 'roots-sass'),
		8 => sprintf( __('Quiz submitted. <a target="_blank" href="%s">Preview quiz</a>', 'roots-sass'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Quiz scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview quiz</a>', 'roots-sass'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Quiz draft updated. <a target="_blank" href="%s">Preview quiz</a>', 'roots-sass'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'quiz_updated_messages' );
