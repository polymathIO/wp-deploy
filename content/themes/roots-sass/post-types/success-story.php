<?php

function success_story_init() {
	register_post_type( 'success-story', array(
		'labels'            => array(
			'name'                => __( 'Success stories' ),
			'singular_name'       => __( 'Success story' ),
			'all_items'           => __( 'Success stories' ),
			'new_item'            => __( 'New success story' ),
			'add_new'             => __( 'Add New' ),
			'add_new_item'        => __( 'Add New success story' ),
			'edit_item'           => __( 'Edit success story' ),
			'view_item'           => __( 'View success story' ),
			'search_items'        => __( 'Search success stories' ),
			'not_found'           => __( 'No success stories found' ),
			'not_found_in_trash'  => __( 'No success stories found in trash' ),
			'parent_item_colon'   => __( 'Parent success story' ),
			'menu_name'           => __( 'Success stories' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
	) );

}
add_action( 'init', 'success_story_init' );

function success_story_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['success-story'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Success story updated. <a target="_blank" href="%s">View success story</a>'), esc_url( $permalink ) ),
		2 => __('Custom field updated.'),
		3 => __('Custom field deleted.'),
		4 => __('Success story updated.'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Success story restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Success story published. <a href="%s">View success story</a>'), esc_url( $permalink ) ),
		7 => __('Success story saved.'),
		8 => sprintf( __('Success story submitted. <a target="_blank" href="%s">Preview success story</a>'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Success story scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview success story</a>'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Success story draft updated. <a target="_blank" href="%s">Preview success story</a>'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'success_story_updated_messages' );
