<?php

function characteristic_init() {
	register_post_type( 'characteristic', array(
		'labels'            => array(
			'name'                => __( 'Characteristics', 'roots-sass' ),
			'singular_name'       => __( 'Characteristic', 'roots-sass' ),
			'all_items'           => __( 'Characteristics', 'roots-sass' ),
			'new_item'            => __( 'New characteristic', 'roots-sass' ),
			'add_new'             => __( 'Add New', 'roots-sass' ),
			'add_new_item'        => __( 'Add New characteristic', 'roots-sass' ),
			'edit_item'           => __( 'Edit characteristic', 'roots-sass' ),
			'view_item'           => __( 'View characteristic', 'roots-sass' ),
			'search_items'        => __( 'Search characteristics', 'roots-sass' ),
			'not_found'           => __( 'No characteristics found', 'roots-sass' ),
			'not_found_in_trash'  => __( 'No characteristics found in trash', 'roots-sass' ),
			'parent_item_colon'   => __( 'Parent characteristic', 'roots-sass' ),
			'menu_name'           => __( 'Characteristics', 'roots-sass' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
	) );

}
add_action( 'init', 'characteristic_init' );

function characteristic_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['characteristic'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Characteristic updated. <a target="_blank" href="%s">View characteristic</a>', 'roots-sass'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'roots-sass'),
		3 => __('Custom field deleted.', 'roots-sass'),
		4 => __('Characteristic updated.', 'roots-sass'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Characteristic restored to revision from %s', 'roots-sass'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Characteristic published. <a href="%s">View characteristic</a>', 'roots-sass'), esc_url( $permalink ) ),
		7 => __('Characteristic saved.', 'roots-sass'),
		8 => sprintf( __('Characteristic submitted. <a target="_blank" href="%s">Preview characteristic</a>', 'roots-sass'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Characteristic scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview characteristic</a>', 'roots-sass'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Characteristic draft updated. <a target="_blank" href="%s">Preview characteristic</a>', 'roots-sass'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'characteristic_updated_messages' );
