<?php

function video_init() {
	register_post_type( 'video', array(
		'labels'            => array(
			'name'                => __( 'Videos', 'roots-sass' ),
			'singular_name'       => __( 'Video', 'roots-sass' ),
			'all_items'           => __( 'Videos', 'roots-sass' ),
			'new_item'            => __( 'New video', 'roots-sass' ),
			'add_new'             => __( 'Add New', 'roots-sass' ),
			'add_new_item'        => __( 'Add New video', 'roots-sass' ),
			'edit_item'           => __( 'Edit video', 'roots-sass' ),
			'view_item'           => __( 'View video', 'roots-sass' ),
			'search_items'        => __( 'Search videos', 'roots-sass' ),
			'not_found'           => __( 'No videos found', 'roots-sass' ),
			'not_found_in_trash'  => __( 'No videos found in trash', 'roots-sass' ),
			'parent_item_colon'   => __( 'Parent video', 'roots-sass' ),
			'menu_name'           => __( 'Videos', 'roots-sass' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
	) );

}
add_action( 'init', 'video_init' );

function video_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['video'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Video updated. <a target="_blank" href="%s">View video</a>', 'roots-sass'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'roots-sass'),
		3 => __('Custom field deleted.', 'roots-sass'),
		4 => __('Video updated.', 'roots-sass'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Video restored to revision from %s', 'roots-sass'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Video published. <a href="%s">View video</a>', 'roots-sass'), esc_url( $permalink ) ),
		7 => __('Video saved.', 'roots-sass'),
		8 => sprintf( __('Video submitted. <a target="_blank" href="%s">Preview video</a>', 'roots-sass'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Video scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview video</a>', 'roots-sass'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Video draft updated. <a target="_blank" href="%s">Preview video</a>', 'roots-sass'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'video_updated_messages' );
