<?php

function tip_init() {
	register_post_type( 'tip', array(
		'labels'            => array(
			'name'                => __( 'Tips', 'roots-sass' ),
			'singular_name'       => __( 'Tip', 'roots-sass' ),
			'all_items'           => __( 'Tips', 'roots-sass' ),
			'new_item'            => __( 'New tip', 'roots-sass' ),
			'add_new'             => __( 'Add New', 'roots-sass' ),
			'add_new_item'        => __( 'Add New tip', 'roots-sass' ),
			'edit_item'           => __( 'Edit tip', 'roots-sass' ),
			'view_item'           => __( 'View tip', 'roots-sass' ),
			'search_items'        => __( 'Search tips', 'roots-sass' ),
			'not_found'           => __( 'No tips found', 'roots-sass' ),
			'not_found_in_trash'  => __( 'No tips found in trash', 'roots-sass' ),
			'parent_item_colon'   => __( 'Parent tip', 'roots-sass' ),
			'menu_name'           => __( 'Tips', 'roots-sass' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
	) );

}
add_action( 'init', 'tip_init' );

function tip_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['tip'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Tip updated. <a target="_blank" href="%s">View tip</a>', 'roots-sass'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'roots-sass'),
		3 => __('Custom field deleted.', 'roots-sass'),
		4 => __('Tip updated.', 'roots-sass'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Tip restored to revision from %s', 'roots-sass'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Tip published. <a href="%s">View tip</a>', 'roots-sass'), esc_url( $permalink ) ),
		7 => __('Tip saved.', 'roots-sass'),
		8 => sprintf( __('Tip submitted. <a target="_blank" href="%s">Preview tip</a>', 'roots-sass'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Tip scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview tip</a>', 'roots-sass'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Tip draft updated. <a target="_blank" href="%s">Preview tip</a>', 'roots-sass'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'tip_updated_messages' );
