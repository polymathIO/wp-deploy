<?php

function health_center_init() {
	register_post_type( 'health-center', array(
		'labels'            => array(
			'name'                => __( 'Health Centers', 'roots-sass' ),
			'singular_name'       => __( 'Health Center', 'roots-sass' ),
			'all_items'           => __( 'Health Centers', 'roots-sass' ),
			'new_item'            => __( 'New health center', 'roots-sass' ),
			'add_new'             => __( 'Add New', 'roots-sass' ),
			'add_new_item'        => __( 'Add New health center', 'roots-sass' ),
			'edit_item'           => __( 'Edit health center', 'roots-sass' ),
			'view_item'           => __( 'View health center', 'roots-sass' ),
			'search_items'        => __( 'Search health centers', 'roots-sass' ),
			'not_found'           => __( 'No health centers found', 'roots-sass' ),
			'not_found_in_trash'  => __( 'No health centers found in trash', 'roots-sass' ),
			'parent_item_colon'   => __( 'Parent health center', 'roots-sass' ),
			'menu_name'           => __( 'Health centers', 'roots-sass' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
	) );

}
add_action( 'init', 'health_center_init' );

function health_center_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['health-center'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Health center updated. <a target="_blank" href="%s">View health center</a>', 'roots-sass'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'roots-sass'),
		3 => __('Custom field deleted.', 'roots-sass'),
		4 => __('Health center updated.', 'roots-sass'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Health center restored to revision from %s', 'roots-sass'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Health center published. <a href="%s">View health center</a>', 'roots-sass'), esc_url( $permalink ) ),
		7 => __('Health center saved.', 'roots-sass'),
		8 => sprintf( __('Health center submitted. <a target="_blank" href="%s">Preview health center</a>', 'roots-sass'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Health center scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview health center</a>', 'roots-sass'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Health center draft updated. <a target="_blank" href="%s">Preview health center</a>', 'roots-sass'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'health_center_updated_messages' );
