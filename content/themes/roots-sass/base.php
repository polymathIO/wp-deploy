<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>

  <!--[if lt IE 8]>
    <div class="alert alert-warning">
      <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
    </div>
  <![endif]-->

  <?php
    do_action('get_header');
    get_template_part('templates/header');

    $context = Timber::get_context();
    $post = new TimberPost();
    $context['post'] = $post;
    Timber::render('lightbox.twig', $context);
    Timber::render('hero.twig', $context);
  ?>

<?php include roots_template_path(); ?>
<?php if (roots_display_sidebar()) : ?>
  <aside class="sidebar" role="complementary">
    <?php include roots_sidebar_path(); ?>
  </aside><!-- /.sidebar -->
<?php endif; ?>
<?php get_template_part('templates/footer'); ?>
<?php wp_footer(); ?>

</body>
</html>
