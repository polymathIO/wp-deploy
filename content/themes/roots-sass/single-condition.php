<?php
/*
Template Name: A-Z Condition Single Page
*/

	$context = Timber::get_context();
	$post = new TimberPost();
	$context['post'] = $post;
?>


<div class="inform-bar wide-bar" id="inform">
	<h1><span></span>INFORM</h1>
</div>
<section class="accordion-section inform">
	<div class="small-bar">
		<ul>
			<li>Description</li>
			<li>Symptoms</li>
			<li>Causes</li>
			<li>Conventional Treatment</li>
			<li>Tests</li>
			<li>Quiz</li>
			<li>Myths</li>
		</ul>
	</div>
	<div class="container">
		<?php Timber::render('condition-description.twig', $context); ?>
		<?php Timber::render('condition-symptoms_causes.twig', $context); ?>
		<?php Timber::render('condition-conventional_treatments.twig', $context); ?>
		<?php Timber::render('condition-tests.twig', $context); ?>
		<?php Timber::render('condition-quiz.twig', $context); ?>
	</div>
	<?php Timber::render('condition-myths.twig', $context); ?>
</section>

<div class="empower-bar wide-bar" id="empower">
<h1><span></span>EMPOWER</h1>
</div>
<section class="accordion-section empower">
</section>

<div class="heal-bar wide-bar" id="heal">
<h1><span></span>HEAL</h1>
</div>
<section class="accordion-section heal">
</section>

<div class="home-sidebar">
  <div class="wrap container">
	<div class="col-md-8">
	  <h1 class="borders">Ask a Question</h1>

	  <h4 class="question-title">Question Title Goes Here - <span class="author">John Doe</span> - <span class="date">February 25, 2014</span></h4>
	  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
	  <a href="#" class="view-replies">View Replies</a>
	  <div class="reply highlighted"><p><div class="block"><span class="reply-author">Jonathan Tommey</span> - <span class="author-date">May 04, 2014</span></div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
	  <div class="block"><a href="#" class="thumbs-up">25</a><a href="#" class="thumbs-down">3</a><a href="#" class="comment-button">Comment</a></p></div>
	</div>
	  <div class="reply"><p><span class="reply-author">Jonathan Tommey</span> - <span class="author-date">May 04, 2014</span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
		<div class="block"><a href="#" class="thumbs-up">25</a><a href="#" class="thumbs-down">3</a><a href="#" class="comment-button">Comment</a></p></div>
	</div>


	  </div>
	<div class="col-md-4 sidebar">
	  <div class="first-widget">
		<div class="col-md-8">
		  <h3>Subscribe to our Newsletter</h3>
		  <div class="input-group input-group-lg">
			<input type="text" class="light form-control" placeholder="EMAIL ADDRESS" />
			<span class="input-group-btn">
			  <button class="btn btn-default btn-light" type="button">Submit</button>
			</span>
		  </div>
		</div>
		<div class="col-md-4">
		  <h3>Share Page</h3>
		  <div class="social-widget">
			<a href="#" class="facebook"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook.svg" height="25px"></a>
			<a href="#" class="twitter"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter.svg" height="25px"></a>
			<a href="#" class="google"><img src="<?php echo get_template_directory_uri(); ?>/img/google.svg" height="25px"></a>
		  </div>
		</div>
	  </div>
	  <div class="practitioner-sidebar">
	  <h3>Find a Practitioner</h3>
	  <div class="btn-group btn-group-block">
		<button type="button" class="btn btn-outline btn-lg">State</button>
		<button type="button" class="btn btn-outline dropdown-toggle btn-lg" data-toggle="dropdown" aria-expanded="false">
		  <span class="caret"></span>
		  <span class="sr-only">Toggle Dropdown</span>
		</button>
		<ul class="dropdown-menu" role="menu">
		  <li><a href="#">Action</a></li>
		  <li><a href="#">Another action</a></li>
		  <li><a href="#">Something else here</a></li>
		  <li class="divider"></li>
		  <li><a href="#">Separated link</a></li>
		</ul>
	  </div>
	  <div class="btn-group btn-group-block">
		<button type="button" class="btn btn-outline btn-lg">Practice</button>
		<button type="button" class="btn btn-outline dropdown-toggle btn-lg" data-toggle="dropdown" aria-expanded="false">
		  <span class="caret"></span>
		  <span class="sr-only">Toggle Dropdown</span>
		</button>
		<ul class="dropdown-menu" role="menu">
		  <li><a href="#">Action</a></li>
		  <li><a href="#">Another action</a></li>
		  <li><a href="#">Something else here</a></li>
		  <li class="divider"></li>
		  <li><a href="#">Separated link</a></li>
		</ul>
	  </div>
	  <div class="btn-group btn-group-block">
		<button type="button" class="btn btn-outline btn-lg">Specialty</button>
		<button type="button" class="btn btn-outline dropdown-toggle btn-lg" data-toggle="dropdown" aria-expanded="false">
		  <span class="caret"></span>
		  <span class="sr-only">Toggle Dropdown</span>
		</button>
		<ul class="dropdown-menu" role="menu">
		  <li><a href="#">Action</a></li>
		  <li><a href="#">Another action</a></li>
		  <li><a href="#">Something else here</a></li>
		  <li class="divider"></li>
		  <li><a href="#">Separated link</a></li>
		</ul>
	  </div>
	  <br/><br/><br/><br/><br/><br/><br/><br/>
	  <a class="btn btn-primary btn-lg go">Go</a>
	</div>
	</div>
  </div>
</div>

<footer class="content-info" role="contentinfo">
  <div class="wrap container">
	<div class="col-md-2">
	  <h5>Navigate</h5>
	  <ul>
		<li>Home</li>
		<li>A-Z Health Conditions</li>
		<li>Find a Practioner</li>
		<li>Reading Room</li>
		<li>Become a Members</li>
		<li>Self Assessment</li>
		<li>Contact</li>
	  </ul>
	</div>
	<div class="col-md-5">
	  <div class="footer-articles">
		<h5>Most Popular Articles</h5>
		<p><img src="<?php echo get_template_directory_uri(); ?>/img/articleimage.png" width="50px" align="left" style="margin-right: 10px;"><strong>Article title here</strong><br/>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p><br/>
		<p><img src="<?php echo get_template_directory_uri(); ?>/img/articleimage.png" width="50px" align="left" style="margin-right: 10px;"><strong>Article title here</strong><br/>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p><br/>
		<p><img src="<?php echo get_template_directory_uri(); ?>/img/articleimage.png" width="50px" align="left" style="margin-right: 10px;"><strong>Article title here</strong><br/>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
	  </div>
	</div>
	<div class="col-md-5">
	  <div class="about-widget">
		<h5>About</h5>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
		<h5>Connect</h5>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
		<div class="col-md-8">
		<div class="input-group input-group-lg">
		  <input type="text" class="dark form-control" placeholder="EMAIL ADDRESS">
		  <span class="input-group-btn">
			<button class="btn btn-default btn-danger" type="button">Submit</button>
			</span>
		</div>
	  </div>
	  <div class="col-md-4">
		<div class="social-widget-bottom">
		  <a href="#" class="facebook"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook-green.svg" height="25px"></a>
		  <a href="#" class="twitter"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter-green.svg" height="25px"></a>
		  <a href="#" class="google"><img src="<?php echo get_template_directory_uri(); ?>/img/google-green.svg" height="25px"></a>
		</div>
	  </div>
	  </div>
	</div>
  </div>
</footer> 
</body>
</html>
