<?php $obj = get_post_type_object( 'health-center' ); ?>

  <div class="green-overlay">
    <div class="container">
      <div class="row">
        <div class="slides">
          <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
            <div>
              <a href="<?php the_permalink(); ?>">
                <div class="slide-image" style="background-image: url(<?php the_field('post-image'); ?>);" ></div>
                <h3><?php the_title(); ?></h3>
                <p>Learn More</p>
              </a>
            </div>
          <?php endwhile; endif; ?>
          </div>
        </div>
        </div>
    </div>
  </div>
</div>

<div class="home-sidebar"> 
  <div class="wrap container">
    <div class="col-md-8">
      <h1 class="borders">Browse <?php echo $obj->labels->name; ?></h1>
        <ul>
          <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
            <li class="col-sm-3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
          <?php endwhile; endif; ?>
        </ul>
      </div>
    <div class="col-md-4 sidebar">
      <div class="first-widget">
        <div class="col-md-8">
          <h3>Subscribe to our Newsletter</h3>
          <div class="input-group input-group-lg">
            <input type="text" class="light form-control" placeholder="EMAIL ADDRESS" />
            <span class="input-group-btn">
              <button class="btn btn-default btn-light" type="button">Submit</button>
            </span>
          </div>
        </div>
        <div class="col-md-4">
          <h3>Share Page</h3>
          <div class="social-widget">
            <a href="#" class="facebook"><img src="/content/themes/roots-sass/img/facebook.svg" height="25px"></a>
            <a href="#" class="twitter"><img src="/content/themes/roots-sass/img/twitter.svg" height="25px"></a>
            <a href="#" class="google"><img src="/content/themes/roots-sass/img/google.svg" height="25px"></a>
          </div>
        </div>
      </div>
      <div class="practitioner-sidebar">
      <h3>Find a Practitioner</h3>
      <div class="btn-group btn-group-block">
        <button type="button" class="btn btn-outline btn-lg">State</button>
        <button type="button" class="btn btn-outline dropdown-toggle btn-lg" data-toggle="dropdown" aria-expanded="false">
          <span class="caret"></span>
          <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu" role="menu">
          <li><a href="#">Action</a></li>
          <li><a href="#">Another action</a></li>
          <li><a href="#">Something else here</a></li>
          <li class="divider"></li>
          <li><a href="#">Separated link</a></li>
        </ul>
      </div>
      <div class="btn-group btn-group-block">
        <button type="button" class="btn btn-outline btn-lg">Practice</button>
        <button type="button" class="btn btn-outline dropdown-toggle btn-lg" data-toggle="dropdown" aria-expanded="false">
          <span class="caret"></span>
          <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu" role="menu">
          <li><a href="#">Action</a></li>
          <li><a href="#">Another action</a></li>
          <li><a href="#">Something else here</a></li>
          <li class="divider"></li>
          <li><a href="#">Separated link</a></li>
        </ul>
      </div>
      <div class="btn-group btn-group-block">
        <button type="button" class="btn btn-outline btn-lg">Specialty</button>
        <button type="button" class="btn btn-outline dropdown-toggle btn-lg" data-toggle="dropdown" aria-expanded="false">
          <span class="caret"></span>
          <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu" role="menu">
          <li><a href="#">Action</a></li>
          <li><a href="#">Another action</a></li>
          <li><a href="#">Something else here</a></li>
          <li class="divider"></li>
          <li><a href="#">Separated link</a></li>
        </ul>
      </div>
      <br/><br/><br/><br/><br/><br/><br/><br/>
      <a class="btn btn-primary btn-lg go">Go</a>
    </div>
    </div>
  </div>
</div>
  <div class="search-conditions">
    <div class="search-conditions-wrap">
    <h1>Search <?php echo $obj->labels->name; ?></h1>
    <div class="input-group input-group-lg">
      <input type="text" class="light form-control" placeholder="">
      <span class="input-group-btn">
        <button class="btn btn-default btn-danger" type="button">Submit</button>
        </span>
    </div>
  </div>
  </div>


  <footer class="content-info" role="contentinfo">
    <div class="wrap container">
      <div class="col-md-2">
        <h5>Navigate</h5>
        <ul>
          <li>Home</li>
          <li>A-Z Health Conditions</li>
          <li>Find a Practioner</li>
          <li>Reading Room</li>
          <li>Become a Members</li>
          <li>Self Assessment</li>
          <li>Contact</li>
        </ul>
      </div>
      <div class="col-md-5">
        <div class="footer-articles">
          <h5>Most Popular Articles</h5>
          <p><img src="/content/themes/roots-sass/img/articleimage.png" width="50px" align="left" style="margin-right: 10px;"><strong>Article title here</strong><br/>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p><br/>
          <p><img src="/content/themes/roots-sass/img/articleimage.png" width="50px" align="left" style="margin-right: 10px;"><strong>Article title here</strong><br/>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p><br/>
          <p><img src="/content/themes/roots-sass/img/articleimage.png" width="50px" align="left" style="margin-right: 10px;"><strong>Article title here</strong><br/>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
        </div>
      </div>
      <div class="col-md-5">
        <div class="about-widget">
          <h5>About</h5>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
          <h5>Connect</h5>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
          <div class="col-md-8">
          <div class="input-group input-group-lg">
            <input type="text" class="dark form-control" placeholder="EMAIL ADDRESS">
            <span class="input-group-btn">
              <button class="btn btn-default btn-danger" type="button">Submit</button>
              </span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="social-widget-bottom">
            <a href="#" class="facebook"><img src="/content/themes/roots-sass/img/facebook-green.svg" height="25px"></a>
            <a href="#" class="twitter"><img src="/content/themes/roots-sass/img/twitter-green.svg" height="25px"></a>
            <a href="#" class="google"><img src="/content/themes/roots-sass/img/google-green.svg" height="25px"></a>
          </div>
        </div>
        </div>
      </div>
    </div>
  </footer>
  <script src="//use.typekit.net/iqg7cyr.js"></script>
  <script>try{Typekit.load();}catch(e){}</script>
  <script type='text/javascript' src='./assets/js/scripts.min.js,q23a10e3676fe2d9d973564dbe26f0b74.pagespeed.jm.VM39ZDaGxc.js'></script>
  <script src="//localhost:12346/livereload.js"></script>
  <script type="text/javascript" src="slick/slick.min.js"></script>
  <script type="text/javascript">
      $('.slides-blog').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 2000,
        arrows: true,
      });
  </script>
  <script>
  tabby.init();
  </script>
</body>
</html>
