<!doctype html>
<html class="no-js" lang="en-US">
  <head>
    <title>user&#039;s Blog!</title>
    <link rel='stylesheet' id='roots_css-css' href='./assets/css/main.css' type='text/css' media='all'/>
    <link rel="stylesheet" type="text/css" href="./slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="./slick/slick-theme.css"/>
    <script type='text/javascript' src='./assets/js/vendor/modernizr.js'></script>
    <script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js'></script>
    <script>window.jQuery||document.write('<script src="./assets/vendor/jquery/dist/jquery.js?1.11.1"><\/script>')</script>
    <link rel="stylesheet" href="css/tabby.css">
    <script src="js/classList.js"></script>
    <script src="js/tabby.js"></script>
  </head>
  <body class="health-center-single">
    <header class="banner navbar navbar-default navbar-static-top" role="banner">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="./">
            <span></span>
            Healthius
            <span></span>
          </a>
        </div>
        <nav class="collapse navbar-collapse" role="navigation">
          <ul id="menu-primary-navigation" class="nav navbar-nav"><li class="menu-how-we-help"><a href="./how-we-help/">How We Help</a></li>
            <li class="menu-health-assessment"><a href="./health-assessment/">Health Assessment</a></li>
            <li class="menu-health-centers"><a href="./health-centers/">Health Centers</a></li>
            <li class="menu-a-z-health-conditions"><a href="./a-z-health-conditions/">A-Z Health Conditions</a></li>
            <li class="menu-find-a-practitioner"><a href="./find-a-practitioner/">Find a Practitioner</a></li>
            <li class="menu-reading-room"><a href="./reading-room/">Reading Room</a></li>
            <li class="menu-store"><a href="./store/">Store</a></li>
            <div class="social-widget-header">
              <a href="#" class="facebook"><img src="/content/themes/roots-sass/img/facebook.svg" height="25px"></a>
              <a href="#" class="twitter"><img src="/content/themes/roots-sass/img/twitter.svg" height="25px"></a>
              <a href="#" class="google"><img src="/content/themes/roots-sass/img/google.svg" height="25px"></a>
            </div>
          </ul>
        </nav>
      </div>
      <nav class="secondary-nav">
        <div class="container">
          <a class="left-subnav" href="#">Home > Health Center > Autism</a>
          <a class="btn btn-primary" href="#">Log In</a>
          <a href="#"><span class="glyphicon glyphicon-send" aria-hidden="true"></span>Contact</a>
          <a href="#"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>Search</a>
          <a href="#"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Join Our Practitioner Network</a>
        </div>
      </nav>
    </header>
    <div id="dynamo" class="lightbox">
      <a class="lightbox-btn glyphicon glyphicon-resize-small"></a>
      <div class="content">
        <div class="container">
          <img class="main" src="http://lorempixel.com/1024/540/food/" alt="" pagespeed_url_hash="3286024812">
        </div>
      </div>
    </div>
