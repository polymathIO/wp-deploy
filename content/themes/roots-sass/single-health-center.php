<?php
/*
Template Name: Health Center Single Page
*/

  $context = Timber::get_context();
  $post = new TimberPost();
  $context['post'] = $post;
?>
    <div class="home-sidebar">
        <div class="wrap container">
          <div class="col-md-8">
            <h1>Autism Overview</h1>
            <div class="overview">
            <img src="<?php the_field('overview_image'); ?>" style="width: 100%; margin-bottom: 2rem;" />
            <?php the_field('overview'); ?>
          </div>
            <?php Timber::render('health_center-tabs.twig', $context); ?>
            <?php Timber::render('health_center-articles.twig', $context); ?>

          </div>
          <div class="col-md-4 sidebar">
            <div class="first-widget">
              <div class="col-md-8">
                <h3>Subscribe to our Newsletter</h3>
                <div class="input-group input-group-lg">
                  <input type="text" class="light form-control" placeholder="EMAIL ADDRESS" />
                  <span class="input-group-btn">
                    <button class="btn btn-default btn-light" type="button">Submit</button>
                  </span>
                </div>
              </div>
              <div class="col-md-4">
                <h3>Share Page</h3>
                <div class="social-widget">
                  <a href="#" class="facebook"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/facebook.svg" height="25px"></a>
                  <a href="#" class="twitter"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/twitter.svg" height="25px"></a>
                  <a href="#" class="google"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/google.svg" height="25px"></a>
                </div>
              </div>
            </div>
            <div class="practitioner-dropdown">
            <h3>Find a Practitioner</h3>
            <div class="btn-group btn-group-block">
              <button type="button" class="btn btn-outline btn-lg">State</button>
              <button type="button" class="btn btn-outline dropdown-toggle btn-lg" data-toggle="dropdown" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>
            <div class="btn-group btn-group-block">
              <button type="button" class="btn btn-outline btn-lg">Practice</button>
              <button type="button" class="btn btn-outline dropdown-toggle btn-lg" data-toggle="dropdown" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>
            <div class="btn-group btn-group-block">
              <button type="button" class="btn btn-outline btn-lg">Specialty</button>
              <button type="button" class="btn btn-outline dropdown-toggle btn-lg" data-toggle="dropdown" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>
          </div>
            <div class="images">
              <?php Timber::render('health_center-sidebar_images.twig', $context); ?>

            </div>
            <div class="third-widget">
            </div>
          </div>
        </div>
    </div>
    <footer class="content-info" role="contentinfo">
      <div class="wrap container">
        <div class="col-md-2">
          <h5>Navigate</h5>
          <ul>
            <li>Home</li>
            <li>A-Z Health Conditions</li>
            <li>Find a Practioner</li>
            <li>Reading Room</li>
            <li>Become a Members</li>
            <li>Self Assessment</li>
            <li>Contact</li>
          </ul>
        </div>
        <div class="col-md-5">
          <div class="footer-articles">
            <h5>Most Popular Articles</h5>
            <p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/articleimage.png" width="50px" align="left" style="margin-right: 10px;"><strong>Article title here</strong><br/>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p><br/>
            <p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/articleimage.png" width="50px" align="left" style="margin-right: 10px;"><strong>Article title here</strong><br/>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p><br/>
            <p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/articleimage.png" width="50px" align="left" style="margin-right: 10px;"><strong>Article title here</strong><br/>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
          </div>
        </div>
        <div class="col-md-5">
          <div class="about-widget">
            <h5>About</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
            <h5>Connect</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
            <div class="col-md-8">
            <div class="input-group input-group-lg">
              <input type="text" class="dark form-control" placeholder="EMAIL ADDRESS">
              <span class="input-group-btn">
                <button class="btn btn-default btn-danger" type="button">Submit</button>
                </span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="social-widget-bottom">
              <a href="#" class="facebook"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/facebook-green.svg" height="25px"></a>
              <a href="#" class="twitter"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/twitter-green.svg" height="25px"></a>
              <a href="#" class="google"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/google-green.svg" height="25px"></a>
            </div>
          </div>
          </div>
        </div>
      </div>
    </footer>
  </body>
</html>
