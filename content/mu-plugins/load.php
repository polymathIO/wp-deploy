<?php
/*
Plugin Name:  Must Use Plugins Loader
Plugin URI:   http://polymath.io/
Description:  Making sure this works!
Version:      1.0
Author:       Daniel Coulbourne
Author URI:   http://polymath.io/
*/

// require WPMU_PLUGIN_DIR . '/my-plugin/my-plugin.php';
require WPMU_PLUGIN_DIR . '/wordpress-importer/wordpress-importer.php';
require WPMU_PLUGIN_DIR . '/acf/acf.php';
require WPMU_PLUGIN_DIR . '/acf-repeater/acf-repeater.php';
require WPMU_PLUGIN_DIR . '/advanced-custom-fields-wpcli/advanced-custom-fields-wpcli.php';
