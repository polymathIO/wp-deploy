############################################
# Setup Server
############################################

set :stage, :staging
set :stage_url, "http://52.11.23.173"
server "52.11.23.173", user: "bitnami", roles: %w{web app db}
set :deploy_to, "/opt/bitnami/apache2/htdocs"
 
############################################
# Setup Git branch
############################################

set :branch, "master"

############################################
# Extra Settings
############################################

#specify extra ssh options:

set :ssh_options, {
    auth_methods: %w(publickey),
    keys: %w(~/.ssh/Badasses.pem),
    user: 'bitnami',
}

#specify a specific temp dir if user is jailed to home
#set :tmp_dir, "/path/to/custom/tmp"
